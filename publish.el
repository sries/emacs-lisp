(require 'org-roam)

(defun benmezger/org-roam-export-all ()
   "Re-exports all Org-roam files to Hugo markdown."
   (interactive)
   (dolist (f (org-roam--list-all-files))
     (with-current-buffer (find-file f)
       (when (s-contains? "SETUPFILE" (buffer-string))
	 (org-hugo-export-wim-to-md)))))
 (defun benmezger/org-roam--backlinks-list (file)
   (when (org-roam--org-roam-file-p file)
     (mapcar #'car (org-roam-db-query [:select :distinct [from]
				       :from links
				       :where (= to $s1)
				       :and from :not :like $s2] file "%private%"))))
 (defun benmezger/org-export-preprocessor (_backend)
   (when-let ((links (benmezger/org-roam--backlinks-list (buffer-file-name))))
     (insert "\n** Backlinks\n")
     (dolist (link links)
       (insert (format "- [[file:%s][%s]]\n"
		       (file-relative-name link org-roam-directory)
		       (org-roam--get-title-or-slug link))))))
 (add-hook 'org-export-before-processing-hook #benmezger/org-export-preprocessor)


(setq org-publish-project-alist
      '(("org-roam"
         :base-directory "~/org/roam"
         :base-extension "org"
	 :publishing-directory "/var/www/org-roam"
         :publishing-function org-html-publish-to-html
         :section-numbers nil
         :with-toc nil
         :html-head "<link rel=\"stylesheet\"
                  href=\"../other/mystyle.css\" type=\"text/css\"/>"
         :html-preamble t)))

(org-publish-all)
