(require 'igrep)

(defvar glimpse-options "")

(defun glimpse (program expression files options)
  (interactive
   (let ((igrep-program "glimpse"))
     (igrep-read-args t)))
  (if (null options)
      (setq options glimpse-options))
  (let ((command (format "%s -n %s %c%s%c"
			 program
			 options
			 igrep-expression-quote-char
			 expression
			 igrep-expression-quote-char)))
			
    (compile-internal command
		      (format "No more %c%s%c matches"
			      igrep-expression-quote-char
			      program
			      igrep-expression-quote-char)
		      "iglimpse" nil grep-regexp-alist)))

(global-set-key (kbd "<S-f2>") (lambda () (interactive)
			       (let ((glimpse-options "-H /vobs/umc_src/ssp"))
				 (call-interactively 'glimpse))))
