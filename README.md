My Emacs customizations
=======================

Usage
------

 - git clone git clone https://sries@bitbucket.org/sries/emacs-lisp.git ~/emacs/lisp
 - symlink ~/emacs/lisp/dotemacs.el to ~/.emacs
 - optionally create ~/.address with single line email address
   (user@example.com)
 - optionally create ~/.emacs-private.el with non-shared customization
 
Customization
--------------

Don't modify ~/.emacs or ~/emacs/lisp/init.el, edit
~/emacs/lisp/init.org instead.

Individual sections can be disabled by setting ":tangle no"

Packages
========

This repo also contains a collection of emacs packages from the time
before ELPA was available. (I may clean that up at some point, don't
hold your breath :-)
